# Django management command imports
import collections as c
import logging
from string import Template

from django.core.management.base import BaseCommand

# Django functionality imports

from django.conf import settings
# Python imports

import os, json

# Logging

logger = logging.getLogger('django')
logger.info('Beginning the command to fetch and load all data.')

# Utilities

from utilities.file_management import *

# Constants

BASE_DIR = settings.BASE_DIR
SOURCE_DATA = settings.SOURCE_DATA
PROPUBLICA_DATA = settings.PROPUBLICA_DATA
OPENSTATES_DATA = settings.OPENSTATES_DATA
OUTPUT_DATA = settings.OUTPUT_DATA


# Management command scaffolding

class Command(BaseCommand):
    help = "Fetch vote history and FEC data from remote sources, load them into PSQL."

    def add_arguments(self, parser):
        parser.add_argument('sources', nargs='*')
        parser.add_argument(
            '--download', '-d', #flag
            action='store_true',
            dest='download',
            default='False',
            help='Specify whether or not to download the data again.'

          )


    # Variables for logic

    def handle(self, *args, **options):

        dispatcher = c.OrderedDict()
        dispatcher['pro_publica'] = fetch_and_load_pro_publica
        dispatcher['open_states'] = fetch_and_load_open_states
        dispatcher['fec'] = fetch_and_load_fec

        download_toggle = options['download']

        successes = []
        failures = []

        logger.info('Download flag not set.')
        if len(options['sources']) == 0:
            # do them all in order
            logger.info("No specific source was given, so all will be executed.")

            try:
                for source in dispatcher:
                    print "Currently working on: {0}".format(source)
                    result = dispatcher[source](download=download_toggle)

                    if (result):
                        successes.append(source)
                    else:
                        failures.append(source)

            except Exception as ex:
                print "There was an error in {0}. \n The error was: {1}".format(source, ex)
        else:
            for source in options['options']:
                try:
                    logger.info("Currently working on: {0}".format(source))
                    result = dispatcher[source](download=download_toggle)

                    if (result):
                        successes.append(source)
                    else:
                        failures.append(source)

                except KeyError as K:
                    logger.error(
                        "Your dataset is not in the accepted list. You entered {0}. Valid choices are: {1}".format(
                            source, dispatcher.keys()))

        logger.info("Successes:{0}".format(successes))
        logger.info("Failures:{0}".format(failures))


def fetch_and_load_pro_publica(download=False):
    """Fetches all ProPublica archived legislation data and creates tables from it in the database"""

    if (download):

    # Step 1: fetch the files from ProPublica and extract them
        logger.info('Downloading the files as specified')
        fetch_and_extract_propublica(first_congress=93, last_congress=114)

    else:
        logger.info('Downloading manually overriden.')

    # Step 2: get all of this shit into a big JSON file that is compressed

    all_legislation = traverse_propublica_and_create_json()

    legislation_output = OUTPUT_DATA+'propublica_legislation.json'

    with open(legislation_output, 'w') as target:
        json.dump(all_legislation, target, indent=4)


def fetch_and_load_open_states(download=False):
    """"Fetches and loads all OpenStates archived data and creates tables from it in the database"""

    return True


def fetch_and_load_fec(download=False):
    """Fetches and loads all FEC data and creates tables from it in the database"""

    return True


def fetch_and_extract_propublica(first_congress=93, last_congress=114):
    """Fetches all ProPublica archived legislation data and unzips the files to a directory structure"""

    link_template = Template("https://s3.amazonaws.com/pp-projects-static/congress/bills/$congress.zip")

    for congress in range(first_congress, last_congress + 1):

        link_to_try = link_template.substitute(congress=congress)
        logger.info(link_to_try)

        zipfile_directory_template = Template(PROPUBLICA_DATA + '/$congress/')
        zipfile_directory = zipfile_directory_template.substitute(congress=congress)
        pretty_mkdir(zipfile_directory)
        file_name = 'pro_publica_congress_summary_for_' + link_to_try.split('/')[-1]
        full_file_path = zipfile_directory + file_name

        try:
            os.chdir(zipfile_directory)
            fetch_data(link_to_try, file_name)
            unzip_file(zipped_target_path=full_file_path, target_dir=zipfile_directory)
        except Exception as Ex:
            logger.error(Ex)
            return False

    return True


def traverse_propublica_and_create_json():


    """Traverses the unique datasets created by fetching and unzipping the archived legislation
    and returns a JSON that reflects the Congress and each different type of legislation"""

    congress_directory_template = Template(PROPUBLICA_DATA + '/$congress/bills')
    legislation_file_path_template = Template(PROPUBLICA_DATA + '/$congress/bills/$leg_type/$bill_id/data.json')

    # JSON that will reflect the full legislative period of two years, i.e, a Congress
    # This is what we will return

    legislation_json = {}

    missing_data_log = PROPUBLICA_DATA+'/missing_data.txt'
    missing_data_file = open(missing_data_log, 'w')


    # change directories to the propublica source directory
    os.chdir(PROPUBLICA_DATA)


    try:
        congresses = [str(integered_congress) for integered_congress in
                      sorted([int(congress) for congress in get_immediate_subdirectories('.')])]

        logger.info("Sorted list of congresses to traverse: {}".format(congresses))

    except Exception as ex:

        logger.error("You're fucked")
        sys.exit()

    for congress in congresses:

        # The zip files for certain congressses have different data structures so we are goint to have to consider that
        logger.info('Traversing the ProPublica directory and building a data structure for {0} Congress'.format(congress))
        if congress in ['113','114']:

            congress_directory_template = Template(PROPUBLICA_DATA +'/$congress/congress/data/$congress/bills')
            legislation_file_path_template = Template(PROPUBLICA_DATA + '/$congress/congress/data/$congress/bills/$leg_type/$bill_id/data.json')


        # this is what we are going to attach to the main legislation_json
        legislation_summary = {}

        # get to the directory of the Congress

        congress_directory = congress_directory_template.substitute(congress=congress)

        os.chdir(congress_directory)

        # now we are going to get all the different legislation types and get each instance thereof

        legislation_types = os.listdir('.')

        for leg_type in legislation_types:

            # we are going to populate this JSON with every piece of legislation of this type

            legislation_type_summary = {}

            # change directory to the one for the leg type

            os.chdir(leg_type)

            # get every piece of legislation in the type

            bills = os.listdir('.')


            for bill in bills:

                legislation_data_file = legislation_file_path_template.substitute(
                    congress=congress,
                    leg_type=leg_type,
                    bill_id=bill
                )

                try:
                    with open(legislation_data_file, 'r') as f:
                        bill_json = json.load(f)

                        # add the individual bills to the type JSON

                        legislation_type_summary[bill] = bill_json

                except Exception as ex:

                    error_message = str(ex) + 'Missing data: {0} \n'.format(legislation_data_file)

                    logger.info(error_message)
                    missing_data_file.write(error_message)


            # add the summary of each legislation type to the summary of the congress

            legislation_summary[leg_type] = legislation_type_summary

            os.chdir(congress_directory)


        # final legislation summary

        legislation_json[congress] = legislation_summary

    missing_data_file.close()
    return legislation_json
