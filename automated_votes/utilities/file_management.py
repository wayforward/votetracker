# Django functionality imports

from django.conf import settings

# Python imports

import sys, os
import urllib2
from zipfile import ZipFile

# Requests related things

import requests
from clint.textui import progress

# Constants

BASE_DIR = settings.BASE_DIR

def fetch_data(file_url, intended_path):

    """This function fetches a file from a remote URL and saves it to a file."""

    print "Now fetching the file from remote location {0}.".format(file_url)

    r = requests.get(file_url, stream=True)

    try:
        with open(intended_path, 'wb') as f:
            total_length = int(r.headers.get('content-length'))
            for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length / 1024) + 1):
                if chunk:
                    f.write(chunk)
                    f.flush()
        print "Successfully saved the file from {0} to {1}".format(file_url, intended_path)
        return True

    except Exception as e:
        print "There was a problem saving the file from {0} to {1}".format(file_url, intended_path)
        return False


def unzip_file(zipped_target_path, target_dir='./'):
    """This function takes us to the Django basedir, unzips the file and redundantly takes us to the Django basedir."""

    print "Attempting to unzip the file at path: {0}".format(zipped_target_path)
    try:
        os.chdir(BASE_DIR)
        os.chdir(target_dir)

        file_to_unzip = zipped_target_path.split('/')[-1]

        target_zip = ZipFile(file_to_unzip)
        target_zip.extractall(target_dir)
        os.chdir(BASE_DIR)

        return True

    except Exception as ex:

        print "There was an error unzipping the file at {0}: {1}".format(zipped_target_path, ex)
        return False


def pretty_mkdir(newdir):


    """
        http://code.activestate.com/recipes/82465-a-friendly-mkdir/
        works the way a good mkdir should :)
        - already exists, silently complete
        - regular file in the way, raise an exception
        - parent directory(ies) does not exist, make them as well
    """
    if os.path.isdir(newdir):
        pass
    elif os.path.isfile(newdir):
        raise OSError("a file with the same name as the desired " \
                      "dir, '%s', already exists." % newdir)
    else:
        head, tail = os.path.split(newdir)
        if head and not os.path.isdir(head):
            pretty_mkdir(head)
        if tail:
            os.mkdir(newdir)

    return True

def get_immediate_subdirectories(path):

    """This function takes a path and returns only the immediate subdirectories beneath it."""

    dir_list = next(os.walk(path))[1]
    return dir_list

def get_all_subdirectories(path):

    """This function takes a path and returns all subdirectories recursively"""

    subdirs = [x[0] for x in os.walk(directory)]
    return subdirs


